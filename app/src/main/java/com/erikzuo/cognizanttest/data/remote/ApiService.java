package com.erikzuo.cognizanttest.data.remote;

/**
 * Created by YifanZuo on 16/2/18.
 */

import com.erikzuo.cognizanttest.data.model.ApiResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * REST API access points
 */
public interface ApiService {

    @GET("/s/2iodh4vg0eortkl/facts.json")
    Observable<ApiResponse> getData();
}