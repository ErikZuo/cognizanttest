package com.erikzuo.cognizanttest.data;

import android.content.Context;

import com.erikzuo.cognizanttest.data.model.ApiResponse;
import com.erikzuo.cognizanttest.data.remote.ApiService;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by YifanZuo on 16/2/18.
 */

public class AppDataRepository implements DataRepository {
    private final Context context;
    private ApiService apiService;

    @Inject
    public AppDataRepository(Context context, ApiService apiService) {
        this.context = context;
        this.apiService = apiService;
    }

    @Override
    public Observable<ApiResponse> getData() {
        return apiService.getData();
    }
}
