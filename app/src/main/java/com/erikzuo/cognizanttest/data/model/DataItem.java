package com.erikzuo.cognizanttest.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YifanZuo on 16/2/18.
 */

public class DataItem {

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String desc;

    @SerializedName("imageHref")
    private String imgUrl;


    public DataItem(String title, String desc, String imgUrl) {
        this.title = title;
        this.desc = desc;
        this.imgUrl = imgUrl;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
