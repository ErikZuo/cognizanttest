package com.erikzuo.cognizanttest.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

/**
 * Created by YifanZuo on 16/2/18.
 */

public class ApiResponse {

    @SerializedName("title")
    private String title;

    @SerializedName("rows")
    private List<DataItem> dataItems;

    public ApiResponse(String title, List<DataItem> dataItems) {
        this.title = title;
        this.dataItems = dataItems;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<DataItem> getDataItems() {
        for (Iterator<DataItem> iterator = dataItems.iterator(); iterator.hasNext();){
            DataItem item = iterator.next();

            if (item.getTitle() == null && item.getDesc() == null && item.getImgUrl() == null){
                iterator.remove();
            }
        }

        return dataItems;
    }

    public void setDataItems(List<DataItem> dataItems) {
        this.dataItems = dataItems;
    }
}
