package com.erikzuo.cognizanttest.data;

import com.erikzuo.cognizanttest.data.model.ApiResponse;

import io.reactivex.Observable;


/**
 * Created by YifanZuo on 16/2/18.
 */

public interface DataRepository {

    Observable<ApiResponse> getData();
}
