package com.erikzuo.cognizanttest.di;


import com.erikzuo.cognizanttest.ui.main.MainActivity;
import com.erikzuo.cognizanttest.ui.main.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by YifanZuo on 16/2/18.
 */

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity contributeMainActivity();
}
