package com.erikzuo.cognizanttest.di;

import android.app.Application;
import android.content.Context;

import com.erikzuo.cognizanttest.data.AppDataRepository;
import com.erikzuo.cognizanttest.data.DataRepository;
import com.erikzuo.cognizanttest.data.remote.ApiService;
import com.erikzuo.cognizanttest.util.rx.AppSchedulerProvider;
import com.erikzuo.cognizanttest.util.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by YifanZuo on 16/2/18.
 */

@Module(includes = ViewModelModule.class)
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }


    @Provides
    @Singleton
    DataRepository provideDataRepository(AppDataRepository appDataRepository) {
        return appDataRepository;
    }

    @Provides
    @Singleton
    ApiService provideApiService() {
        return new Retrofit.Builder()
                .baseUrl("https://dl.dropboxusercontent.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService.class);
    }

}
