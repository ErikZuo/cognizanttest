
package com.erikzuo.cognizanttest.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;

import com.erikzuo.cognizanttest.BR;
import com.erikzuo.cognizanttest.R;
import com.erikzuo.cognizanttest.databinding.ActivityMainBinding;
import com.erikzuo.cognizanttest.ui.base.BaseActivity;
import com.erikzuo.cognizanttest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;


/**
 * Created by YifanZuo on 16/2/18.
 */

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator, SwipeRefreshLayout
        .OnRefreshListener {

    @Inject
    ViewModelProviderFactory mFactory;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    ItemAdapter mItemAdapter;


    private MainViewModel mViewModel;

    @Override
    public MainViewModel getViewModel() {
        mViewModel = ViewModelProviders.of(this, mFactory).get(MainViewModel.class);
        return mViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initViews() {
        mViewModel.setNavigator(this);

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        getViewDataBinding().list.setLayoutManager(mLayoutManager);
        getViewDataBinding().list.setItemAnimator(new DefaultItemAnimator());
        getViewDataBinding().list.setAdapter(mItemAdapter);

        getViewDataBinding().refreshLayout.setOnRefreshListener(this);

        subscribeToLiveData();
    }


    private void subscribeToLiveData() {
        mViewModel.getApiLiveData().observe(
                this,
                apiResponse -> mViewModel.updateUi(apiResponse));
    }

    @Override
    public void onRefresh() {
        mViewModel.loadData();
    }

    @Override
    public void setRefreshing(boolean isRefreshing) {
        getViewDataBinding().refreshLayout.setRefreshing(isRefreshing);
    }

}
