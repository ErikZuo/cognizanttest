package com.erikzuo.cognizanttest.ui.main;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.erikzuo.cognizanttest.data.model.DataItem;
import com.erikzuo.cognizanttest.util.AppLogger;

/**
 * Created by YifanZuo on 16/2/18.
 */

public class ItemViewModel extends ViewModel{
    private DataItem dataItem;

    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> description = new ObservableField<>();
    public ObservableField<String> imgUrl = new ObservableField<>();

    public ObservableBoolean titleVisible = new ObservableBoolean(true);
    public ObservableBoolean descVisible = new ObservableBoolean(true);
    public ObservableBoolean imgVisible = new ObservableBoolean(true);


    public ItemViewModel(DataItem dataItem) {
        this.dataItem = dataItem;

        title.set(this.dataItem.getTitle());
        description.set(this.dataItem.getDesc());
        imgUrl.set(this.dataItem.getImgUrl());

        titleVisible.set(title.get() != null);
        descVisible.set(description.get() != null);
        imgVisible.set(dataItem.getImgUrl() != null);

    }
}
