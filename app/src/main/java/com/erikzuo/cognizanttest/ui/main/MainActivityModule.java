package com.erikzuo.cognizanttest.ui.main;

import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;


/**
 * Created by YifanZuo on 16/2/18.
 */

@Module
public class MainActivityModule {

    @Provides
    ItemAdapter provideItemAdapter(){
        return new ItemAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(MainActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
