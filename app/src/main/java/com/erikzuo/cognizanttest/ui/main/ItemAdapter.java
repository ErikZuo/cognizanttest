package com.erikzuo.cognizanttest.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.erikzuo.cognizanttest.data.model.DataItem;
import com.erikzuo.cognizanttest.databinding.RowItemBinding;
import com.erikzuo.cognizanttest.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by YifanZuo on 16/2/18.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private List<DataItem> mDataList;

    public ItemAdapter(List<DataItem> rowList) {
        this.mDataList = rowList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowItemBinding rowItemBinding = RowItemBinding.inflate(
                LayoutInflater.from(parent.getContext()),
                parent,
                false);

        return new ViewHolder(rowItemBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }


    public void addItems(List<DataItem> dataItems) {
        mDataList.addAll(dataItems);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends BaseViewHolder {
        private RowItemBinding mBinding;
        private ItemViewModel mViewModel;

        public ViewHolder(RowItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final DataItem item = mDataList.get(position);

            mViewModel = new ItemViewModel(item);
            mBinding.setViewModel(mViewModel);
            mBinding.executePendingBindings();
        }
    }
}
