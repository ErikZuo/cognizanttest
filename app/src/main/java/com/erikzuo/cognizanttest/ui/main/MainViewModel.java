package com.erikzuo.cognizanttest.ui.main;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;

import com.erikzuo.cognizanttest.data.DataRepository;
import com.erikzuo.cognizanttest.data.model.ApiResponse;
import com.erikzuo.cognizanttest.data.model.DataItem;
import com.erikzuo.cognizanttest.ui.base.BaseViewModel;
import com.erikzuo.cognizanttest.util.rx.SchedulerProvider;

import javax.inject.Inject;


/**
 * Created by YifanZuo on 16/2/18.
 */

public class MainViewModel extends BaseViewModel<MainNavigator> {

    private final ObservableField<String> mTitle = new ObservableField<>();
    private final ObservableField<String> mMessage = new ObservableField<>();
    private final ObservableArrayList<DataItem> mItemList = new ObservableArrayList<>();

    private final MutableLiveData<ApiResponse> mApiLiveData = new MutableLiveData<>();

    @Inject
    public MainViewModel(DataRepository dataRepository, SchedulerProvider schedulerProvider) {
        super(dataRepository, schedulerProvider);

        loadData();
    }

    /**
     * Load data from REST API
     */
    public void loadData() {
        startLoading();
        getCompositeDisposable().add(getDataRepository().getData()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(
                        apiResponse -> {
                            mApiLiveData.setValue(apiResponse);
                            finishLoading();
                        },
                        throwable -> {
                            mMessage.set(throwable.getLocalizedMessage());
                            finishLoading();
                        }
                ));
    }


    /*******************************************
     *  Getters for data binding
     *******************************************/

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    public ObservableArrayList<DataItem> getItemList() {
        return mItemList;
    }

    public ObservableField<String> getMessage() {
        return mMessage;
    }


    /*******************************************
     *  Public method for communication between livedata and databindind fields
     *******************************************/

    public MutableLiveData<ApiResponse> getApiLiveData() {
        return mApiLiveData;
    }

    public void updateUi(ApiResponse apiResponse) {
        mTitle.set(apiResponse.getTitle());

        mItemList.clear();
        mItemList.addAll(apiResponse.getDataItems());
    }


    /*******************************************
     *  Utils
     *******************************************/

    private void startLoading() {
        if (getNavigator() == null) {
            setIsLoading(true);
        } else {
            getNavigator().setRefreshing(true);
        }
    }

    private void finishLoading() {
        setIsLoading(false);
        if (getNavigator() != null) {
            getNavigator().setRefreshing(false);
        }
    }
}
