package com.erikzuo.cognizanttest.ui.main;

/**
 * Created by YifanZuo on 17/2/18.
 */

public interface MainNavigator {

    void setRefreshing(boolean isRefreshing);

}
