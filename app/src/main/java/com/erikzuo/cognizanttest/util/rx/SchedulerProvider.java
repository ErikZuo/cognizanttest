package com.erikzuo.cognizanttest.util.rx;

import io.reactivex.Scheduler;


/**
 * Created by YifanZuo on 16/2/18.
 */

public interface SchedulerProvider {

    Scheduler ui();

    Scheduler computation();

    Scheduler io();

}
