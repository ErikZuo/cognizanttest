package com.erikzuo.cognizanttest.util;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.erikzuo.cognizanttest.R;
import com.erikzuo.cognizanttest.data.model.DataItem;
import com.erikzuo.cognizanttest.ui.main.ItemAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Utils class for custom data binding attributes.
 *
 * Created by YifanZuo on 16/2/18.
 */


public class BindingUtils {

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        if (url == null) {
            imageView.setImageDrawable(null);
        } else {
            Glide.with(imageView.getContext())
                    .load(url)
                    .apply(new RequestOptions().error(R.drawable.ic_img_broken))
                    .into(imageView);
        }
    }


    @BindingAdapter({"adapter"})
    public static void addRepoItems(RecyclerView recyclerView,
                                    ArrayList<DataItem> dataItems) {
        ItemAdapter adapter = (ItemAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(dataItems);
        }
    }
}
