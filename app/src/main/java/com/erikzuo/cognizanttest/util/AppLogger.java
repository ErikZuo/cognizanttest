package com.erikzuo.cognizanttest.util;

import com.erikzuo.cognizanttest.BuildConfig;

import timber.log.Timber;

/**
 * Created by YifanZuo on 16/2/18.
 */

public final class AppLogger {

    private AppLogger() {
        // This utility class is not publicly instantiable
    }

    public static void init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static void d(String s, Object... objects) {
        Timber.d(s, objects);
    }

}