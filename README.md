# Cognizant Coding Test #

### Summary ###

This repository is created for completing a Android coding test for Cognizant.

 
### Techniques used in this project ###

* MVVM design pattern
* Dagger 2 for dependency injection
* Android architecture component such as databinding, liveData and viewModel for maintaining application lifecycle